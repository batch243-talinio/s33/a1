	const toDoList = [];

	fetch('https://jsonplaceholder.typicode.com/todos')
    .then((response) => response.json())
    .then((json) => {
	        json.map((elem) => {
	            toDoList.push(elem.title);
	        }
        );
    });

    console.log(toDoList);



    fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then((response) => response.json())
    .then((json) => console.log(json))



    fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then((response) => response.json())
    .then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))



    fetch('https://jsonplaceholder.typicode.com/todos', {

	    method: 'POST',
	    headers: {
	            'Content-type': 'application/json',
	    },
	    body: JSON.stringify({
            completed: false,
            id: 201,
            title: "Created To Do List Item",
            userId: 1
	        
	    })
	})
    .then((response) => response.json())
    .then((json) => console.log(json));



	fetch('https://jsonplaceholder.typicode.com/todos/1',{

		method: 'PUT',
		headers: {
			'Content-Type':'application/json'
		},

		body: JSON.stringify({
            completed: false,
            id: 1,
            title: "Updated To Do List Item",
            userId: 1
	        
	    })
	})
	.then((response) => response.json())
	// .then((jopay) => console.log(jopay))



	fetch('https://jsonplaceholder.typicode.com/todos/1',{
			method: 'PUT',
			headers: {
				'Content-Type':'application/json',
			},
			body: JSON.stringify({
				dateCompleted: 'Pending...',
				description: 'To update the my to do list with a different data structute.',
				id: 1,
				status: 'Pending...',
				title: 'Updated To Do List Item',
				userId: 1
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))



	fetch('https://jsonplaceholder.typicode.com/todos/1',{
			method: 'PATCH',
			headers: {
				'Content-Type':'application/json',
			},
			body: JSON.stringify({
				completed: false,
				dateCompleted: '07/09/21',
				id: 1,
				status: 'Complete',
				title: 'delectus aut autem',
				userId: 1
			})
		})
		.then((response) => response.json())
		.then((json) => console.log(json))



	fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: "DELETE"
	})